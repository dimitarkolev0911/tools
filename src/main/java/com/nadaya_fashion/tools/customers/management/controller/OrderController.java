package com.nadaya_fashion.tools.customers.management.controller;

import com.nadaya_fashion.tools.customers.management.Order;
import com.nadaya_fashion.tools.customers.management.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/get-all")
    public Set<Order> getAllOrders() {
        return this.orderService.getOrders();
    }
}
