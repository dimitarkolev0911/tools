package com.nadaya_fashion.tools.customers.management;

import lombok.Builder;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
@Builder
public class Customer {

    private UUID id;
    private String name;
    private String phoneNumber;
    private Set<Order> orders;

    public double calculateTotalAmount() {
        return orders.stream()
                .filter(order -> !order.isPaid())
                .mapToDouble(order -> order.getPrice().doubleValue()).sum();
    }

}
