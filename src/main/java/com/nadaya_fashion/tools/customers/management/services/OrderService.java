package com.nadaya_fashion.tools.customers.management.services;

import com.nadaya_fashion.tools.customers.management.Order;
import com.nadaya_fashion.tools.customers.management.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Set<Order> getOrders() {
        return this.orderRepository.getOrders();
    }

    public Set<Order> getOrdersByCustomerName(String customerName) {
        return this.orderRepository.getOrdersByCustomerName(customerName);
    }

    public void addOrder(Order order) {
        this.orderRepository.addOrder(order);
    }

    public void updateOrder(Order order) {
        this.orderRepository.updateOrder(order);
    }

    public void markOrderAsPaid(UUID orderId) {
        Order order = this.orderRepository.getOrderById(orderId).orElseThrow(() -> new IllegalArgumentException("There is no order with such id!"));
        order.setPaid(true);
        updateOrder(order);
    }
}
