package com.nadaya_fashion.tools.customers.management.repository;

import com.nadaya_fashion.tools.customers.management.Customer;
import com.nadaya_fashion.tools.customers.management.Order;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static java.math.BigDecimal.valueOf;

@Repository
public class OrderRepository {

    List<Order> orders = this.getInitialOrders();

    public Set<Order> getOrders() {
        return new HashSet<>(this.orders);
    }

    public Set<Order> getOrdersByCustomerName(String customerName) {
        return new HashSet<>(this.orders.stream().filter(order -> customerName.equals(order.getCustomer().getName())).collect(Collectors.toSet()));
    }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public Optional<Order> getOrderById(UUID orderId) {
        return this.orders.stream().filter(o -> orderId.equals(o.getId())).findFirst();
    }

    public void updateOrder(Order order) {
        this.orders.stream().filter(o -> order.getId().equals(o.getId())).forEach(o -> {
            o.setCustomer(order.getCustomer());
            o.setPaid(order.isPaid());
            o.setNumberOfProductsBought(order.getNumberOfProductsBought());
            o.setPrice(order.getPrice());
        });
    }

    private List<Order> getInitialOrders() {
        Customer customer = Customer.builder().id(UUID.randomUUID()).name("Димитър Хасково").orders(new HashSet<>()).phoneNumber("+359879123456").build();
        Customer customer2 = Customer.builder().id(UUID.randomUUID()).name("Румен Стара Загора").orders(new HashSet<>()).phoneNumber("+359884123456").build();

        return new ArrayList<>(List.of(
                Order.builder().id(UUID.randomUUID()).customer(customer).dateCreated(new Date()).price(valueOf(1200)).numberOfProductsBought(60).build(),
                Order.builder().id(UUID.randomUUID()).customer(customer).dateCreated(new Date()).price(valueOf(1000)).numberOfProductsBought(30).build(),
                Order.builder().id(UUID.randomUUID()).customer(customer).dateCreated(new Date()).price(valueOf(300)).numberOfProductsBought(10).build(),
                Order.builder().id(UUID.randomUUID()).customer(customer2).dateCreated(new Date()).price(valueOf(1000)).numberOfProductsBought(50).build(),
                Order.builder().id(UUID.randomUUID()).customer(customer2).dateCreated(new Date()).price(valueOf(2000)).numberOfProductsBought(120).build()
        ));
    }
}
