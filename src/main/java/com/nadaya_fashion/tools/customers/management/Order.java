package com.nadaya_fashion.tools.customers.management;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
public class Order {

    private UUID id;
    private BigDecimal price;
    private Integer numberOfProductsBought;
    private Date dateCreated;
    private Customer customer;
    private boolean isPaid;

}
